#!/bin/bash
export RASA_HOME=/opt/rasa

set -Eeuo pipefail

#######################################
#
# installing ansible and roles
#
#######################################

source /etc/os-release

RUN_ANSIBLE=${RUN_ANSIBLE:-true}

echo "Installing pip and ansible"

if [[ $ID =~ centos|rhel ]]; then

    sudo yum update -y
    sudo yum install -y python3
    sudo yum install -y python3-distutils || :  # if the package is not available and the command fails, do nothing,
                                                    # the distutils are already installed
elif [[ $ID =~ ubuntu|debian ]]; then
    sudo apt-get update -y
    sudo apt-get install -y python3
    sudo apt-get install -y docker.io
    sudo apt-get install -y docker-compose
    sudo apt-get install -y python3-distutils || :  # if the package is not available and the command fails, do nothing,
                                                       # the distutils are already installed
    sudo apt-get install -y wget || :  # wget may already be installed; do nothing if it is
    
elif [[ $ID =~ fedora ]]; then

    sudo dnf update -y
    sudo dnf install -y python3
    sudo dnf install -y python3-distutils-extra || :  # if the package is not available and the command fails, do nothing,
                                                    # the distutils are already installed
fi

curl -O https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
sudo /usr/local/bin/pip install "ansible>-2.9, <2.10"

echo "Installing docker role"
sudo /usr/local/bin/ansible-galaxy install geerlingguy.docker
echo "Docker role for ansible has been installed"

sudo mkdir /opt/rasa
cd /opt/rasa
wget https://bitbucket.org/danilonl/new-public-repo/raw/19b13a3e96df7792f5a5c0c625cdf7133d4d2b1b/.env
wget https://bitbucket.org/danilonl/new-public-repo/raw/19b13a3e96df7792f5a5c0c625cdf7133d4d2b1b/credentials.yml
wget https://bitbucket.org/danilonl/new-public-repo/raw/19b13a3e96df7792f5a5c0c625cdf7133d4d2b1b/docker-compose.yml
wget https://bitbucket.org/danilonl/new-public-repo/raw/19b13a3e96df7792f5a5c0c625cdf7133d4d2b1b/endpoints.yml
wget https://bitbucket.org/danilonl/new-public-repo/raw/19b13a3e96df7792f5a5c0c625cdf7133d4d2b1b/environments.yml
read -p "Server IP: " serverip
sed -i 's/192\.168\.177\.242/192\.168\.177\.244/g' endpoints.yml
docker network create rasa-net
sudo docker-compose up -d
docker-compose ps

cd /opt/
git clone git@bitbucket.org:e-core/cesp-chatbot.git || git clone https://danilonl@bitbucket.org/e-core/cesp-chatbot.git
git clone git@bitbucket.org:e-core/cesp-rasa-actions.git || git clone https://danilonl@bitbucket.org/e-core/cesp-rasa-actions.git

cd /opt/cesp-chatbot
sed -i 's/chatbot-net/rasa-net/g' docker-compose.yml
mv docker-compose.override.yml docker-compose.override.yml.BKP
sed -i 's/ -f docker-compose\.override\.yml / /g' compose.sh
cp .env.staging .env
sed -i 's/10\.10\.8\.96/192\.168\.177\.244/g' .env

./compose.sh up -d
docker-compose ps

cd /opt/cesp-rasa-actions
cp .env.sample .env
docker-compose up -d
docker-compose ps