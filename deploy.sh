#!/bin/bash

cd /opt/
git clone git@bitbucket.org:e-core/cesp-chatbot.git || git clone https://danilonl@bitbucket.org/e-core/cesp-chatbot.git
git clone git@bitbucket.org:e-core/cesp-rasa-actions.git || git clone https://danilonl@bitbucket.org/e-core/cesp-rasa-actions.git

cd /opt/cesp-chatbot
mv docker-compose.override.yml docker-compose.override.yml.BKP
sed -i 's/ -f docker-compose\.override\.yml / /g' compose.sh
cp .env.staging .env

cd /opt/cesp-rasa-actions
cp .env.sample .env

cd /opt

wget https://bitbucket.org/danilonl/new-public-repo/raw/bc1a284ac4bf0ef75532ca372324273e11dbfe03/config.sh
sudo chmod +x config.sh

echo " "
echo " "
echo "Rode o comando:  /opt/config.sh  para terminar a configuração"
echo " "
