#!/bin/bash
echo "List of IPs:"
ip address show | grep -v inet6 | grep inet | sed -e 's/ \{2,\}/ /g' | cut -d ' ' -f 3 | grep -v 127.0.0.1 | cut -d '/' -f 1

read -p "IP address of RASA (one of the above?): " rasaip
read -p "Docker Network Name: " netwname
sudo sed -i "s/chatbot-net/${netwname}/g" /opt/cesp-chatbot/docker-compose.yml
sudo sed -i "s/10\.10\.8\.96/${rasaip}/g" /opt/cesp-chatbot/.env

cd /opt/cesp-chatbot
./compose.sh up -d
docker-compose ps

cd /opt/cesp-rasa-actions
docker-compose up -d
docker-compose ps